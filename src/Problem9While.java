import java.util.Scanner;

public class Problem9While {
    public static void main(String[] args) {
        System.out.print("Please input n: ");
        Scanner sc = new Scanner(System.in);
        int count = sc.nextInt();
        int i=0;
        while(i<count){
            int j=1;
            while(j<=count){
                System.out.print(j);
                j++;
            }
            System.out.println();
            i++;
        }
    }
    
}
